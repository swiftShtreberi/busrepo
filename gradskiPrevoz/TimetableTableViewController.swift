//
//  TimetableTableViewController.swift
//  gradskiPrevoz
//
//  Created by Dusan Juranovic on 1/23/17.
//  Copyright © 2017 Dusan Juranovic. All rights reserved.
//

import UIKit
import GoogleMaps

class TimetableTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {

    @IBOutlet var timetableTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }

    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Table view data source

     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return stanice.count
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "timetableCell", for: indexPath)
        cell.textLabel?.text = "Long: \(stanice[indexPath.row].coordinates), Lat: \(stanice[indexPath.row])"
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Stanica \(section)"
    }
   
}
