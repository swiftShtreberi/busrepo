//
//  ViewController.swift
//  gradskiPrevoz
//
//  Created by Dusan Juranovic on 1/23/17.
//  Copyright © 2017 Dusan Juranovic. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

//func + (left: NSAttributedString, right: NSAttributedString) -> NSAttributedString {
//    let result = NSMutableAttributedString()
//    result.append(left)
//    result.append(right)
//    return result
//}

extension UIView{
    
    
    func addCornerRadiusAnimation(from: CGFloat, to: CGFloat, duration: CFTimeInterval){
        let animation = CABasicAnimation(keyPath:"cornerRadius")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.fromValue = from
        animation.toValue = to
        animation.duration = duration
        self.layer.add(animation, forKey: "cornerRadius")
        self.layer.cornerRadius = to
     
    }
}

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, GMSMapViewDelegate, CLLocationManagerDelegate{
    
    @IBOutlet var makeASelection: UILabel!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var pickerView: UIPickerView!
    @IBOutlet var beginButton: UIView!
    @IBOutlet var beginButtonLabel: UILabel!
    @IBOutlet var mapView: UIView!
    @IBOutlet var prikaziButton: UIButton!
    
    
    var marker: GMSMarker?
    var googleMapView: GMSMapView!
    
    var locationManager = CLLocationManager()
    
    var tapGesture = UITapGestureRecognizer()
    
    var odabraniRed1 = 0
    var odabraniRed2 = 0
    var odabraniRed3 = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        readJson()
        makeASelection.alpha = 0
        pickerView.alpha = 0
        searchBar.alpha = 0
        prikaziButton.alpha = 0
        
        locationManager.delegate = self
        
        beginButton.layer.cornerRadius = 25
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(beginAction))
        beginButton.addGestureRecognizer(tapGesture)
        
        let camera = GMSCameraPosition.camera(withLatitude: 44.787197, longitude: 20.457273, zoom: 10)
        googleMapView = GMSMapView.map(withFrame: mapView.bounds, camera: camera)
        googleMapView.isMyLocationEnabled = true
        googleMapView.settings.compassButton = true
        googleMapView.settings.myLocationButton = true
        googleMapView.camera = camera
        googleMapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        googleMapView.delegate = self
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        mapView.addSubview(googleMapView)
        mapView.alpha = 0
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    @IBAction func prikaziAction(_ sender: UIButton) {
        //Prikazati odabrane koordinate na mapi
        
        marker?.map?.clear()
        for var sta in stanice {
            if sta.lineType == "\(vrstaPrevoza[pickerView.selectedRow(inComponent: 0)])" {
                if sta.lineNumber == "\(linije[pickerView.selectedRow(inComponent: 1)])"{
                    
                    for koord in sta.coordinates {
                        let position = CLLocationCoordinate2DMake(koord.lat, koord.lon)
                        
                        marker = GMSMarker(position: position)
                        marker?.map = googleMapView
                        
                        marker?.title = "\(sta.name) \(sta.from)-\(sta.to) \(sta.lineNumber) \(sta.lineType) "
                    }
                }
            }
        }
    }
    func readJson() {
        
        if let path = Bundle.main.path(forResource: "RoadMap", ofType: "txt") {
            do {
                let file = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
                let jData = file.data(using: String.Encoding.utf8)
                do {
                    let json = try JSONSerialization.jsonObject(with: jData!, options: []) as![String: Any]
                    
                    //print(json)
                    let features = json["features"] as! [[String: Any]]
                    // print(features)
                    for feature in features {
                        //print(feature)
                        let geometry = feature["geometry"] as! [String:Any]
                        let id = feature["id"] as! String
                        //print(geometry)
                        let type = geometry["type"] as? String
                        
                        let properties = feature["properties"] as? [String:Any]
                        //print(properties)
                        let name = properties?["name"] as? String ?? ""
                        let pubTrans = properties?["public_transport"] as? String ?? ""
                        let railway = properties?["railway"] as? String ?? ""
                        let ref = properties?["ref"] as? String ?? ""
                        
                        if let relations = properties?["@relations"] as? [[String:Any]] {
                            
                            for relation in relations {
                                var niz = [String]()
                                let reltags = relation["reltags"] as! [String:Any]
                                
                                let lineNumber = reltags["ref"] as? String ?? ""
                                let from = reltags["from"] as? String ?? ""
                                let to = reltags["to"] as? String ?? ""
                                let reltype = reltags["type"] as? String ?? ""
                                let lineType = reltags["route"] as? String ?? ""
                                let amenity = reltags["amenity"] as? String ?? ""
                                let highway = reltags["highway"] as? String ?? ""
                                let shelter = reltags["shelter"] as? String ?? "no"
                                let publicTransport = reltags["public_transport"] as? String ?? ""
                                let railway = reltags["railway"] as? String ?? ""
                                let wheelchair = reltags["wheelchair"] as? String ?? "no"
                                let bench = reltags["bench"] as? String ?? "no"
                                let covered = reltags["covered"] as? String ?? "no"
                                let network = reltags["network"] as? String ?? ""
                                let note = reltags["note"] as? String ?? ""
                                let operators = reltags["operator"] as? String ?? ""
                                let role = reltags["role"] as? String ?? ""
                                
                                if let coordinates = geometry["coordinates"] as? [Double] {
                                    
                                    let lon = coordinates[0]
                                    let lat = coordinates[1]
                                    
                                    coords = Coordinates(lon: lon, lat: lat)
                                    koordinate.append(coords!)
                                    
                                }
                            
                                let stanica = Stanica(id: id, name: name, from: from, to: to, lineNumber:lineNumber, amenity: amenity, coordinates: koordinate, role: role, highway: highway, shelter: shelter, publicTransport: publicTransport, railway: railway, wheelchair: wheelchair, bench: bench, covered: covered, network: network, note: note, operators: operators, lineType: lineType)
                                stanice.append(stanica)
                                koordinate = []
                                niz = []
                            }
                        }
                    }
                } catch {
                    print("Bad json")
                }
            }catch {
                print("No go")
            }
        } else {
            print("No File")
        }
    }
    func beginAction() {
        let timetableButton = UIButton()
        let locateMeButton = UIButton()
        let settingButton = UIButton()
        timetableButton.setTitle("Timetable", for: .normal)
        locateMeButton.setTitle("Locate Me", for: .normal)
        settingButton.setTitle("Settings", for: .normal)
        
        beginButton.addSubview(timetableButton)
        beginButton.addSubview(locateMeButton)
        beginButton.addSubview(settingButton)
        
        UIView.animate(withDuration: 2, animations: {
            self.beginButton.addCornerRadiusAnimation(from: 25, to: 0, duration: 2)
            
            self.beginButton.frame.origin = CGPoint(x: self.view.frame.origin.x, y: self.view.frame.size.height - 50)
            self.beginButton.frame.size = CGSize(width: self.view.frame.width, height: 50)
            self.beginButton.backgroundColor = .white
            self.beginButtonLabel.text = ""
            self.beginButton.layer.cornerRadius = 0
            
            timetableButton.frame = CGRect(x: 10, y: 10 , width: 100, height: 30)
            locateMeButton.frame = CGRect(x: self.beginButton.center.x - 50, y: 10, width: 100, height: 30)
            settingButton.frame = CGRect(x: self.beginButton.frame.maxX - 110, y: 10, width: 100, height: 30)
            
            timetableButton.setTitleColor(UIColor.darkGray, for: .normal)
            locateMeButton.setTitleColor(UIColor.darkGray, for: .normal)
            settingButton.setTitleColor(UIColor.darkGray, for: .normal)
            
            timetableButton.addTarget(self, action: #selector(self.toTable), for: .touchUpInside)
            self.beginButton.removeGestureRecognizer(self.tapGesture)
            
        }, completion: {_ in
            
            UIView.animate(withDuration: 1, animations: {
                self.mapView.alpha = 1
                self.makeASelection.alpha = 0.75
                self.pickerView.alpha = 0.75
                self.searchBar.alpha = 0.75
                self.prikaziButton.alpha = 0.75
            })
        })
    }
    
    func toTable() {
        performSegue(withIdentifier: "toTable", sender: self)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 3
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return vrstaPrevoza.count
        } else if component == 1 {
            return linije.count
        } else if component == 2 {
            return smer.count
        }else {
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let labelView = UILabel()
        
        labelView.adjustsFontSizeToFitWidth = true
        
        if component == 0 {
            let ispis1 = NSAttributedString(string: vrstaPrevoza[row], attributes: [NSFontAttributeName: UIFont(name: "Verdana", size: 13)!, NSForegroundColorAttributeName: UIColor.darkGray])
            labelView.textAlignment = .center
            labelView.attributedText = ispis1
            return labelView
        } else if component == 1 {
            let ispis2 = NSAttributedString(string: linije[row], attributes: [NSFontAttributeName: UIFont(name: "Verdana", size: 13)!, NSForegroundColorAttributeName: UIColor.gray])
            labelView.textAlignment = .center
            labelView.attributedText = ispis2
            return labelView
        } else {
            let ispis3 = NSAttributedString(string: smer[row], attributes: [NSFontAttributeName: UIFont(name: "Verdana", size: 13)!, NSForegroundColorAttributeName: UIColor.darkGray])
            labelView.textAlignment = .center
            labelView.attributedText = ispis3
            return labelView
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if component == 0 {
            odabraniRed1 = row
        } else if component == 1 {
            odabraniRed2 = row
        } else if component == 2 {
            odabraniRed3 = row
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        if component == 0 {
            return pickerView.frame.width * 0.30
        } else if component == 1 {
            return pickerView.frame.width * 0.15
        } else  if component == 2{
            return pickerView.frame.width * 0.45
        } else {
            return 0
        }
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
        let label = UILabel()
        label.frame = CGRect(x: marker.position.latitude, y: marker.position.longitude, width: 200, height: 70)
        label.textAlignment = .center
        label.backgroundColor = UIColor.gray
        label.alpha = 0.75
        label.textColor = .white
        label.numberOfLines = 0
        label.layer.borderColor = UIColor.white.cgColor
        label.layer.borderWidth = 2
        label.adjustsFontSizeToFitWidth = true
        label.text = marker.title
        return label
        
    }
}

