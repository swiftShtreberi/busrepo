//
//  DataModel.swift
//  gradskiPrevoz
//
//  Created by Dusan Juranovic on 1/23/17.
//  Copyright © 2017 Dusan Juranovic. All rights reserved.
//

import Foundation

var vrstaPrevoza = ["train", "bus", "tram"]
var linije = ["2", "3", "5", "7","9","10","11","13"]
var smer = ["Ustanicka - Blok 45", "Blok 45 - Ustanicka", "Studentski Trg - Uciteljsko Naselje", "Uciteljsko Naselje - Studentski Trg"]

var stanice = [Stanica]()

struct Stanica {
    var id: String
    var name: String
    var from: String
    var to: String
    var lineNumber: String
    var amenity: String
    var coordinates: [Coordinates]
    var role: String
    var highway: String
    var shelter: String
    var publicTransport: String
    var railway: String
    var wheelchair: String
    var bench: String
    var covered: String
    var network: String
    var note: String
    var operators: String
    var lineType: String
}

//struct Stanica {
//    
//    
//    var from: String
//    var to: String
//    var coordinates: [Coordinates]
//    var ref: String
//    var type: String
//    var route: String
//}
var coords: Coordinates?
var koordinate = [Coordinates]()

struct Coordinates {
    var lon: Double
    var lat: Double
}

